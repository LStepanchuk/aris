function AuthorizationController() {
    var self = this;

	// "auth_dr[0]":  333,
    // "auth_dr[1]":  334,
    // "auth_dr[2]":  335,
    // "auth_dr[3]":  336,
    // "auth_cr":     337,
	
	var authCr = null;
	var authDr = null;
	var disconnectCallbacks = [];
	var intervalHandle = 0;
	
	var routine = function(){
		if (authCr.GetArray(1) != 1){
			clearInterval(intervalHandle);
			for(var k in disconnectCallbacks)
				disconnectCallbacks[k]();
		}
	};
	
    self.Initialize = function() {
		authCr = paramController.Param("auth_cr");
		authDr = paramController.Param("auth_dr[0]");
		
		authCr.SetArray([2]);
    };
	
	self.Authorize = function(login, pass){
		var md5Data = MD5(login + pass);
		var uintData = new Uint32Array(md5Data,0,4);
		
		authDr.SetArray(uintData);
		authCr.SetArray([1]);
		if (authCr.GetArray(1)[0] == 1) {
			intervalHandle = setInterval(routine,4000);
			return true;
		}
		
		return false;
	};
	
	self.ChangeCredentials = function(oldLogin, oldPass, newLogin, newPass){
		var md5Data = MD5(oldLogin + oldPass);
		var uintData = new Uint32Array(md5Data,0,4);
		
		authDr.SetArray(uintData);
		authCr.SetArray([1]);
		if (authCr.GetArray(1)[0] == 1) {
			var md5NewData = MD5(newLogin + newPass);
			var uintNewData = new Uint32Array(md5NewData,0,4);
			
			authDr.SetDefArray(uintNewData);			
			authCr.SetArray([2]);
			
			return true;
		}
		
		return false;
	};
	
	self.RegisterDisconnectCallback = function(func){
		disconnectCallbacks.push(func);
	};

    return self;
}