﻿function crossBrowsingFeatures(){
	if (!Uint8Array.prototype.filter){
		Uint8Array.prototype.filter = Array.prototype.filter;
		Uint32Array.prototype.filter = Array.prototype.filter;
		Int32Array.prototype.filter = Array.prototype.filter;
	}
}

crossBrowsingFeatures();

var uiController = new UIController();
var requestsController = new RequestsController();
var dataController = new DataProcessingController();
var paramController = null;
var oscillographController = null;
var firmwareController = null;
var analyzerController = null;
var authorizationController = null;
//another controllers, widgets, etc.

$(document).ready(function() {// page started 	
	// Intialization
		
	var factory = new DescriptorFactory();
	for(var i = 0; i < 3; i++){
		var descriptor = factory.Create(i);
		if (descriptor == null){
			uiController.Alert("Invalid descriptor " + i);
			return;
		}
		
		
		switch(descriptor.name){
			case "@FW Storage":
				firmwareController = new FirmwareController(descriptor);
				break;
			case "@ParamTable":
				paramController = new ParamController(descriptor);
				break;
			case "@Oscilloscope":
				oscillographController = new OscillographController(descriptor);
				break;
		}
	}
	
	paramController.Initialize();
	
	oscillographController.Initialize();
	oscillographController.SetInterval(0.6);
	
	analyzerController = new AnalyzerController();
	analyzerController.Initialize();
	
	authorizationController = new AuthorizationController();
	authorizationController.Initialize();
	
	// TODO: Replace 
	// setTimeout(function(){
	// }, 500);
	
    uiController.HtmlInitElements();
});


function loadSource(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref!="undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}
