function DataProcessingController() {
    var self = this;
    var ARISCP_MAX_DATALEN = 1024;

    function ChanelItem(name, gain, sw) {
        this.name = name;
        this.gain = gain; //float
        this.switch = sw;
    }

    function UserData(name, pass) {
        this.name = name;
        this.password = pass;
    }

    function NetworkConfig(ip, mask, gateway, port, mac) {
        this.ip = ip;
        this.mask = mask;
        this.gateway = gateway;
        this.tcpPort = port;
        this.macAddress = mac;
    }

	
	function SingleFilter(key, data){
		this.key = key;
		this.data =  data;
	}
	
	var FiltersSet = [	0.1,0.2,0.4,0.5,	0.1,0.2,0.4,0.5,	0.1,0.2,0.4,0.5,	0.1,0.2,0.4,0.5,
						0.1,0.2,0.4,0.5,	0.1,0.2,0.4,0.5,	0.1,0.2,0.4,0.5,	0.1,0.2,0.4,0.5,];
	
	
	var OscilloscopeGraphicData = [];

    var currentNetworkConfig = new NetworkConfig("192.168.0.52", "255.255.255.0", "192.168.0.1", 77777, ["01", "23", "45", "67", "89", "ab"]);
    var currentUser = new UserData("admin", "admin");

    var ChanelsSet = [
        new ChanelItem("ch0", 1, true),
        new ChanelItem("ch1", 1, true),
        new ChanelItem("ch2", 1, true),
        new ChanelItem("ch3", 1, true),
        new ChanelItem("ch4", 1, true),
        new ChanelItem("ch5", 1, true),
        new ChanelItem("ch6", 1, true),
        new ChanelItem("ch7", 1, true)
    ];
	
	
	var filterChannels = {};
	
	this.CreateFiltersSet = function(){
		for(var i=0; i<8; i++){
		   filterChannels[i] = paramController.Param("ch" + i + "_cf_N[0]");
		}
	}
	
	this.SetFiltersData = function(channel, set, memoryType){
		filterChannels[i].SetArray(set);
		//TODO: send to device
	}
	
	
	this.GetFiltersSet = function(channel){
		return filterChannels[channel].GetArray(32);	}

    this.GetUserData = function() {
        return currentUser;
    }
	
    this.SetUserData = function(name, pass) {
        currentUser.name = name;
        currentUser.password = pass;
    }
	
	this.GetOscilloscopeGraphicData = function(){
		return OscilloscopeGraphicData;
	}

    this.GetNetworkConfig = function() {
        return currentNetworkConfig;
    }
    this.SetNetworkConfig = function(ip, mask, gateway, port, mac) {
        currentNetworkConfig.ip = ip;
        currentNetworkConfig.mask = mask;
        currentNetworkConfig.gateway = gateway;
        currentNetworkConfig.tcpPort = port;
        currentNetworkConfig.macAddress = mac;
    }

    this.GetChanelsSet = function() {
        return ChanelsSet;
    }

    this.UpdateChanelsSet = function(data) {
        $.each(data, function(i, val) {

        })

    }

    this.UpdateChanelsSetItem = function(chanelName, key, valOld, valNew) {
        $.each(ChanelsSet, function(i, item) {
            for (var fieldKey in item) {
                if ((fieldKey == key) && (item[key] == valOld) && (item.name == chanelName)) {
                    item[key] = valNew;
                }
                //localdata = GetLocalData(key);
            }
        })
    }

    this.str2ab = function(str) {
        var buf = new ArrayBuffer(str.length);
        var bufView = new Uint8Array(buf);

        for (var i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i) & 0xff;
        }
        return buf;
    }
	
	this.float2ab = function(f) {
		var farr = new Float32Array([f]);
		return (new Uint8Array(farr.buffer,0,1)).buffer;
    }
	
	this.int2ab = function(i) {
		var farr = new Int32Array([i]);
		return (new Uint8Array(farr.buffer,0,1)).buffer;
    }
	
	this.uint2ab = function(i) {
		var farr = new Uint32Array([i]);
		return (new Uint8Array(farr.buffer,0,1)).buffer;
    }

    this.ab2str = function(buf)	{
		var str = new Uint8Array(buf);
		return String.fromCharCode.apply(null, str);
	}
	
	this.ab2float = function(ab, offset) {
		offset = offset || 0;
		return (new Float32Array(ab,offset,1))[0];
    }
	
	this.ab2int = function(ab, offset) {
		offset = offset || 0;
		return (new Int32Array(ab,offset,1))[0];
    }
	
	this.ab2uint = function(ab, offset) {
		offset = offset || 0;
		return (new Uint32Array(ab,offset,1))[0];
    }
	
	this.ab2afloat = function(ab, offset) {
		offset = offset || 0;
		var size = Math.floor(ab.byteLength / 4);
		return new Float32Array(ab,offset,size);
    }
	
	this.ab2aint = function(ab, offset) {
		offset = offset || 0;
		var size = Math.floor(ab.byteLength / 4);
		return new Int32Array(ab,offset,size);
    }

    this.appendab = function(buffer1, buffer2) {
        var tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
        tmp.set(new Uint8Array(buffer1), 0);
        tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
        return tmp.buffer;
    }

    this.CheckData = function(nAddress, arrData) {
        if ((typeof nAddress != "number") || (arrData.length > ARISCP_MAX_DATALEN))
            return true;
    }

    this.CheckFullData = function(nAddress, nSize) {
        if ((typeof nAddress != "number") || (typeof nSize != "number") || nSize > ARISCP_MAX_DATALEN)
            return true;
    }

    this.StartConnection = function() {
        /* for (var i = 0;; ++i) {
            var str = requestsController.Dev_ReadDescriptorString(i);

            if (str.byteLength == 0)
                break;

            console.log("Dev_ReadDescriptorString(" + i + ") : " + str);
        }

        var buf = requestsController.Dev_ReadLongData(0x40000000, 4096);
        // buf = appendab(buf, requestsController.Dev_ReadData(0x40000000 + 1024, 1024))
        // buf = appendab(buf, requestsController.Dev_ReadData(0x40000000 + 1024 + 1024, 1024));
        // buf = appendab(buf, requestsController.Dev_ReadData(0x40000000 + 1024 + 1024 + 1024, 1024));

        //var bufView = new Uint8Array(buf);
		var bufView = self.ab2afloat(buf);
		OscilloscopeGraphicData = [];
		for(var k=0; k < bufView.byteLength / 4; k++){
			OscilloscopeGraphicData[k] = bufView[k];
		}

        console.log("Dev_ReadData() : ");

        //for (var i = 0, len = bufView.length; i < len; i++)
		//    console.log(bufView[i].toString(16));

        var data = new Uint32Array(1);
        data[0] = 0;

        console.log("Dev_WriteData() : " + requestsController.Dev_WriteData(0x200006B4, data)); // auth_cr */
    }
}