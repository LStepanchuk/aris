function DescriptorFactory() {
    var self = this;
	
	var parseDescriptorString = function(str) {
		var nameRegex = /@[\w\s]+/;
		var ptrRegex = /0x[0-9]+/; 
		var sizeRegex = /[0-9][0-9][0-9][bKM]/; 
		
		var descriptorName = nameRegex.exec(str);
		if (descriptorName == null){
			return null;
		}
		descriptorName = descriptorName[0];
		
		var descriptorPointer = ptrRegex.exec(str);
		if (descriptorPointer == null){
			return null;
		}
		descriptorPointer = descriptorPointer[0];
		
		var descriptorSize = sizeRegex.exec(str);
		if (descriptorSize == null){
			return null;
		}
		descriptorSize = descriptorSize[0];
		
		var descriptorSizeValue = +descriptorSize.substr(0,3);
		
		switch(descriptorSize.substr(3)){
			case "K":
				descriptorSizeValue = descriptorSizeValue*1024;
				break;
			case "M":
				descriptorSizeValue = descriptorSizeValue*1024*1024;
				break;
		}
		
		return {
			name: descriptorName,
			pointer: parseInt(descriptorPointer, 16),
			size: descriptorSizeValue
		};
	}
	
	self.Create = function(i){
		var data = requestsController.Dev_ReadDescriptorString(i);
		var str = dataController.ab2str(data);
		return parseDescriptorString(str);
	};
}

//requestsController.Dev_ReadData = function(nAddress, nSize)
//requestsController.Dev_WriteData = function(nAddress, arrData)
//dataController

function ParamController(desc){
    var self = this;
	
	if (desc.name != "@ParamTable")
		return null;
	
	var i2ab = dataController.int2ab;
	var ui2ab = dataController.uint2ab;
	var f2ab = dataController.float2ab;
	
	var ab2i = dataController.ab2int;
	var ab2ui = dataController.ab2uint;
	var ab2f = dataController.ab2float;
	
	var ab2ai = dataController.ab2aint;
	var ab2af = dataController.ab2afloat;
	
	var read = requestsController.Dev_ReadData;
	var write = requestsController.Dev_WriteData;
	
	var accessAndTypeInfo;
	
	var paramNameDictionary = {
    // ch0: cf_N[]
    "ch0_cf_N[0]": 0,
    "ch0_cf_N[1]": 1,
    "ch0_cf_N[2]": 2,
    "ch0_cf_N[3]": 3,
    "ch0_cf_N[4]": 4,
    "ch0_cf_N[5]": 5,
    "ch0_cf_N[6]": 6,
    "ch0_cf_N[7]": 7,
    // ch0: cf_D
    "ch0_cf_D[0]": 8,
    "ch0_cf_D[1]": 9,
    "ch0_cf_D[2]": 10,
    "ch0_cf_D[3]": 11,
    "ch0_cf_D[4]": 12,
    "ch0_cf_D[5]": 13,
    "ch0_cf_D[6]": 14,
    "ch0_cf_D[7]": 15,
    // ch0: Q_N
    "ch0_Q_N[0]":  16,
    "ch0_Q_N[1]":  17,
    "ch0_Q_N[2]":  18,
    "ch0_Q_N[3]":  19,
    "ch0_Q_N[4]":  20,
    "ch0_Q_N[5]":  21,
    "ch0_Q_N[6]":  22,
    "ch0_Q_N[7]":  23,
    // ch0: Q_D
    "ch0_Q_D[0]":  24,
    "ch0_Q_D[1]":  25,
    "ch0_Q_D[2]":  26,
    "ch0_Q_D[3]":  27,
    "ch0_Q_D[4]":  28,
    "ch0_Q_D[5]":  29,
    "ch0_Q_D[6]":  30,
    "ch0_Q_D[7]":  31,

    // ch1: cf_N[]
    "ch1_cf_N[0]": 32,
    "ch1_cf_N[1]": 33,
    "ch1_cf_N[2]": 34,
    "ch1_cf_N[3]": 35,
    "ch1_cf_N[4]": 36,
    "ch1_cf_N[5]": 37,
    "ch1_cf_N[6]": 38,
    "ch1_cf_N[7]": 39,
    // ch1: cf_D
    "ch1_cf_D[0]": 40,
    "ch1_cf_D[1]": 41,
    "ch1_cf_D[2]": 42,
    "ch1_cf_D[3]": 43,
    "ch1_cf_D[4]": 44,
    "ch1_cf_D[5]": 45,
    "ch1_cf_D[6]": 46,
    "ch1_cf_D[7]": 47,
    // ch1: Q_N
    "ch1_Q_N[0]":  48,
    "ch1_Q_N[1]":  49,
    "ch1_Q_N[2]":  50,
    "ch1_Q_N[3]":  51,
    "ch1_Q_N[4]":  52,
    "ch1_Q_N[5]":  53,
    "ch1_Q_N[6]":  54,
    "ch1_Q_N[7]":  55,
    // ch1: Q_D
    "ch1_Q_D[0]":  56,
    "ch1_Q_D[1]":  57,
    "ch1_Q_D[2]":  58,
    "ch1_Q_D[3]":  59,
    "ch1_Q_D[4]":  60,
    "ch1_Q_D[5]":  61,
    "ch1_Q_D[6]":  62,
    "ch1_Q_D[7]":  63,

    // ch2: cf_N[]
    "ch2_cf_N[0]": 64,
    "ch2_cf_N[1]": 65,
    "ch2_cf_N[2]": 66,
    "ch2_cf_N[3]": 67,
    "ch2_cf_N[4]": 68,
    "ch2_cf_N[5]": 69,
    "ch2_cf_N[6]": 70,
    "ch2_cf_N[7]": 71,
    // ch2: cf_D
    "ch2_cf_D[0]": 72,
    "ch2_cf_D[1]": 73,
    "ch2_cf_D[2]": 74,
    "ch2_cf_D[3]": 75,
    "ch2_cf_D[4]": 76,
    "ch2_cf_D[5]": 77,
    "ch2_cf_D[6]": 78,
    "ch2_cf_D[7]": 79,
    // ch2: Q_N
    "ch2_Q_N[0]":  80,
    "ch2_Q_N[1]":  81,
    "ch2_Q_N[2]":  82,
    "ch2_Q_N[3]":  83,
    "ch2_Q_N[4]":  84,
    "ch2_Q_N[5]":  85,
    "ch2_Q_N[6]":  86,
    "ch2_Q_N[7]":  87,
    // ch2: Q_D
    "ch2_Q_D[0]":  88,
    "ch2_Q_D[1]":  89,
    "ch2_Q_D[2]":  90,
    "ch2_Q_D[3]":  91,
    "ch2_Q_D[4]":  92,
    "ch2_Q_D[5]":  93,
    "ch2_Q_D[6]":  94,
    "ch2_Q_D[7]":  95,

    // ch3: cf_N[]
    "ch3_cf_N[0]": 96,
    "ch3_cf_N[1]": 97,
    "ch3_cf_N[2]": 98,
    "ch3_cf_N[3]": 99,
    "ch3_cf_N[4]": 100,
    "ch3_cf_N[5]": 101,
    "ch3_cf_N[6]": 102,
    "ch3_cf_N[7]": 103,
    // ch3: cf_D
    "ch3_cf_D[0]": 104,
    "ch3_cf_D[1]": 105,
    "ch3_cf_D[2]": 106,
    "ch3_cf_D[3]": 107,
    "ch3_cf_D[4]": 108,
    "ch3_cf_D[5]": 109,
    "ch3_cf_D[6]": 110,
    "ch3_cf_D[7]": 111,
    // ch3: Q_N
    "ch3_Q_N[0]":  112,
    "ch3_Q_N[1]":  113,
    "ch3_Q_N[2]":  114,
    "ch3_Q_N[3]":  115,
    "ch3_Q_N[4]":  116,
    "ch3_Q_N[5]":  117,
    "ch3_Q_N[6]":  118,
    "ch3_Q_N[7]":  119,
    // ch3: Q_D
    "ch3_Q_D[0]":  120,
    "ch3_Q_D[1]":  121,
    "ch3_Q_D[2]":  122,
    "ch3_Q_D[3]":  123,
    "ch3_Q_D[4]":  124,
    "ch3_Q_D[5]":  125,
    "ch3_Q_D[6]":  126,
    "ch3_Q_D[7]":  127,

    // ch4: cf_N[]
    "ch4_cf_N[0]": 128,
    "ch4_cf_N[1]": 129,
    "ch4_cf_N[2]": 130,
    "ch4_cf_N[3]": 131,
    "ch4_cf_N[4]": 132,
    "ch4_cf_N[5]": 133,
    "ch4_cf_N[6]": 134,
    "ch4_cf_N[7]": 135,
    // ch4: cf_D
    "ch4_cf_D[0]": 136,
    "ch4_cf_D[1]": 137,
    "ch4_cf_D[2]": 138,
    "ch4_cf_D[3]": 139,
    "ch4_cf_D[4]": 140,
    "ch4_cf_D[5]": 141,
    "ch4_cf_D[6]": 142,
    "ch4_cf_D[7]": 143,
    // ch4: Q_N
    "ch4_Q_N[0]":  144,
    "ch4_Q_N[1]":  145,
    "ch4_Q_N[2]":  146,
    "ch4_Q_N[3]":  147,
    "ch4_Q_N[4]":  148,
    "ch4_Q_N[5]":  149,
    "ch4_Q_N[6]":  150,
    "ch4_Q_N[7]":  151,
    // ch4: Q_D
    "ch4_Q_D[0]":  152,
    "ch4_Q_D[1]":  153,
    "ch4_Q_D[2]":  154,
    "ch4_Q_D[3]":  155,
    "ch4_Q_D[4]":  156,
    "ch4_Q_D[5]":  157,
    "ch4_Q_D[6]":  158,
    "ch4_Q_D[7]":  159,

    // ch5: cf_N[]
    "ch5_cf_N[0]": 160,
    "ch5_cf_N[1]": 161,
    "ch5_cf_N[2]": 162,
    "ch5_cf_N[3]": 163,
    "ch5_cf_N[4]": 164,
    "ch5_cf_N[5]": 165,
    "ch5_cf_N[6]": 166,
    "ch5_cf_N[7]": 167,
    // ch5: cf_D
    "ch5_cf_D[0]": 168,
    "ch5_cf_D[1]": 169,
    "ch5_cf_D[2]": 170,
    "ch5_cf_D[3]": 171,
    "ch5_cf_D[4]": 172,
    "ch5_cf_D[5]": 173,
    "ch5_cf_D[6]": 174,
    "ch5_cf_D[7]": 175,
    // ch5: Q_N
    "ch5_Q_N[0]":  176,
    "ch5_Q_N[1]":  177,
    "ch5_Q_N[2]":  178,
    "ch5_Q_N[3]":  179,
    "ch5_Q_N[4]":  180,
    "ch5_Q_N[5]":  181,
    "ch5_Q_N[6]":  182,
    "ch5_Q_N[7]":  183,
    // ch5: Q_D
    "ch5_Q_D[0]":  184,
    "ch5_Q_D[1]":  185,
    "ch5_Q_D[2]":  186,
    "ch5_Q_D[3]":  187,
    "ch5_Q_D[4]":  188,
    "ch5_Q_D[5]":  189,
    "ch5_Q_D[6]":  190,
    "ch5_Q_D[7]":  191,

    // ch6: cf_N[]
    "ch6_cf_N[0]": 192,
    "ch6_cf_N[1]": 193,
    "ch6_cf_N[2]": 194,
    "ch6_cf_N[3]": 195,
    "ch6_cf_N[4]": 196,
    "ch6_cf_N[5]": 197,
    "ch6_cf_N[6]": 198,
    "ch6_cf_N[7]": 199,
    // ch6: cf_D
    "ch6_cf_D[0]": 200,
    "ch6_cf_D[1]": 201,
    "ch6_cf_D[2]": 202,
    "ch6_cf_D[3]": 203,
    "ch6_cf_D[4]": 204,
    "ch6_cf_D[5]": 205,
    "ch6_cf_D[6]": 206,
    "ch6_cf_D[7]": 207,
    // ch6: Q_N
    "ch6_Q_N[0]":  208,
    "ch6_Q_N[1]":  209,
    "ch6_Q_N[2]":  210,
    "ch6_Q_N[3]":  211,
    "ch6_Q_N[4]":  212,
    "ch6_Q_N[5]":  213,
    "ch6_Q_N[6]":  214,
    "ch6_Q_N[7]":  215,
    // ch6: Q_D
    "ch6_Q_D[0]":  216,
    "ch6_Q_D[1]":  217,
    "ch6_Q_D[2]":  218,
    "ch6_Q_D[3]":  219,
    "ch6_Q_D[4]":  220,
    "ch6_Q_D[5]":  221,
    "ch6_Q_D[6]":  222,
    "ch6_Q_D[7]":  223,

    // ch7: cf_N[]
    "ch7_cf_N[0]": 224,
    "ch7_cf_N[1]": 225,
    "ch7_cf_N[2]": 226,
    "ch7_cf_N[3]": 227,
    "ch7_cf_N[4]": 228,
    "ch7_cf_N[5]": 229,
    "ch7_cf_N[6]": 230,
    "ch7_cf_N[7]": 231,
    // ch7: cf_D
    "ch7_cf_D[0]": 232,
    "ch7_cf_D[1]": 233,
    "ch7_cf_D[2]": 234,
    "ch7_cf_D[3]": 235,
    "ch7_cf_D[4]": 236,
    "ch7_cf_D[5]": 237,
    "ch7_cf_D[6]": 238,
    "ch7_cf_D[7]": 239,
    // ch7: Q_N
    "ch7_Q_N[0]":  240,
    "ch7_Q_N[1]":  241,
    "ch7_Q_N[2]":  242,
    "ch7_Q_N[3]":  243,
    "ch7_Q_N[4]":  244,
    "ch7_Q_N[5]":  245,
    "ch7_Q_N[6]":  246,
    "ch7_Q_N[7]":  247,
    // ch7: Q_D
    "ch7_Q_D[0]":  248,
    "ch7_Q_D[1]":  249,
    "ch7_Q_D[2]":  250,
    "ch7_Q_D[3]":  251,
    "ch7_Q_D[4]":  252,
    "ch7_Q_D[5]":  253,
    "ch7_Q_D[6]":  254,
    "ch7_Q_D[7]":  255,

    // Gain
    "gain[0]":     256,
    "gain[1]":     257,
    "gain[2]":     258,
    "gain[3]":     259,
    "gain[4]":     260,
    "gain[5]":     261,
    "gain[6]":     262,
    "gain[7]":     263,

    // Available from version 0.24

    // Switch
    "switch[0]":   264,
    "switch[1]":   265,
    "switch[2]":   266,
    "switch[3]":   267,
    "switch[4]":   268,
    "switch[5]":   269,
    "switch[6]":   270,
    "switch[7]":   271,

    // Analyzer
    "analyzer_run_ch":             272, // Count channel from 1 (1-8 == ServoOut; 9-16 == ServoIn), 0 means not to start analyzer.
    "analyzer_rec_count":          273,
    "analyzer_rec_no":             274,

    "analyzer_rec_freq[0]":        275,
    "analyzer_rec_freq[1]":        276,
    "analyzer_rec_freq[2]":        277,
    "analyzer_rec_freq[3]":        278,
    "analyzer_rec_freq[4]":        279,
    "analyzer_rec_freq[5]":        280,
    "analyzer_rec_freq[6]":        281,
    "analyzer_rec_freq[7]":        282,
    "analyzer_rec_freq[8]":        283,
    "analyzer_rec_freq[9]":        284,
    "analyzer_rec_freq[10]":       285,
    "analyzer_rec_freq[11]":       286,
    "analyzer_rec_freq[12]":       287,
    "analyzer_rec_freq[13]":       288,
    "analyzer_rec_freq[14]":       289,
    "analyzer_rec_freq[15]":       290,

    "analyzer_rec_amp[0]":         291,
    "analyzer_rec_amp[1]":         292,
    "analyzer_rec_amp[2]":         293,
    "analyzer_rec_amp[3]":         294,
    "analyzer_rec_amp[4]":         295,
    "analyzer_rec_amp[5]":         296,
    "analyzer_rec_amp[6]":         297,
    "analyzer_rec_amp[7]":         298,
    "analyzer_rec_amp[8]":         299,
    "analyzer_rec_amp[9]":         300,
    "analyzer_rec_amp[10]":        301,
    "analyzer_rec_amp[11]":        302,
    "analyzer_rec_amp[12]":        303,
    "analyzer_rec_amp[13]":        304,
    "analyzer_rec_amp[14]":        305,
    "analyzer_rec_amp[15]":        306,

    "analyzer_rec_phase[0]":       307,
    "analyzer_rec_phase[1]":       308,
    "analyzer_rec_phase[2]":       309,
    "analyzer_rec_phase[3]":       310,
    "analyzer_rec_phase[4]":       311,
    "analyzer_rec_phase[5]":       312,
    "analyzer_rec_phase[6]":       313,
    "analyzer_rec_phase[7]":       314,
    "analyzer_rec_phase[8]":       315,
    "analyzer_rec_phase[9]":       316,
    "analyzer_rec_phase[10]":      317,
    "analyzer_rec_phase[11]":      318,
    "analyzer_rec_phase[12]":      319,
    "analyzer_rec_phase[13]":      320,
    "analyzer_rec_phase[14]":      321,
    "analyzer_rec_phase[15]":      322,

    // Available from version 0.25
    "ch0_name":    323,
    "ch1_name":    324,
    "ch2_name":    325,
    "ch3_name":    326,
    "ch4_name":    327,
    "ch5_name":    328,
    "ch6_name":    329,
    "ch7_name":    330,

    "oscil_ch":    331, // 0-7 == FilerOut; 8-15 == DCBlockOut
    "oscil_ts":    332, // Timestamp for sample buffer

    // Available from version 0.30

    "auth_dr[0]":  333,
    "auth_dr[1]":  334,
    "auth_dr[2]":  335,
    "auth_dr[3]":  336,
    "auth_cr":     337,

    // Available from version 0.33

    "net_ip":      338,
    "net_mask":    339,
    "net_gateway": 340,
    "net_port":    341,
    "net_mac_0":   342,
    "net_mac_1":   343,

    // Available from version 0.35
    "SYS_STATUS_HALL[0]":          344,
    "SYS_STATUS_HALL[1]":          345,
    "SYS_STATUS_HALL[2]":          346,
    "SYS_STATUS_HALL[3]":          347,

    "SYS_STATUS_ADC[0]":           348,
    "SYS_STATUS_ADC[1]":           349,
    "SYS_STATUS_ADC[2]":           350,
    "SYS_STATUS_ADC[3]":           351,
    "SYS_STATUS_ADC[4]":           352,
    "SYS_STATUS_ADC[5]":           353,
    "SYS_STATUS_ADC[6]":           354,
    "SYS_STATUS_ADC[7]":           355,

    "SYS_STATUS_DAC[0]":           356,
    "SYS_STATUS_DAC[1]":           357,
    "SYS_STATUS_DAC[2]":           358,
    "SYS_STATUS_DAC[3]":           359,
    "SYS_STATUS_DAC[4]":           360,
    "SYS_STATUS_DAC[5]":           361,
    "SYS_STATUS_DAC[6]":           362,
    "SYS_STATUS_DAC[7]":           363,
	
	"post_cr":	364,
	"post_st":	365,
	
	"post_gain[0]":	366,
	"post_gain[1]":	367,
	"post_gain[2]":	368,
	"post_gain[3]":	369,
	"post_gain[4]":	370,
	"post_gain[5]":	371,
	"post_gain[6]":	372,
	"post_gain[7]":	373,
	
	"post_phase[0]":	374,
	"post_phase[1]":	375,
	"post_phase[2]":	376,
	"post_phase[3]":	377,
	"post_phase[4]":	378,
	"post_phase[5]":	379,
	"post_phase[6]":	380,
	"post_phase[7]":	381,
	
	"post_gain_wnd_min[0]":	382,
	"post_gain_wnd_min[1]":	383,
	"post_gain_wnd_min[2]":	384,
	"post_gain_wnd_min[3]":	385,
	"post_gain_wnd_min[4]":	386,
	"post_gain_wnd_min[5]":	387,
	"post_gain_wnd_min[6]":	388,
	"post_gain_wnd_min[7]":	389,
	"post_gain_wnd_max[0]":	390,
	"post_gain_wnd_max[1]":	391,
	"post_gain_wnd_max[2]":	392,
	"post_gain_wnd_max[3]":	393,
	"post_gain_wnd_max[4]":	394,
	"post_gain_wnd_max[5]":	395,
	"post_gain_wnd_max[6]":	396,
	"post_gain_wnd_max[7]":	397,
	
	"post_phase_wnd_min[0]":	398,
	"post_phase_wnd_min[1]":	399,
	"post_phase_wnd_min[2]":	400,
	"post_phase_wnd_min[3]":	401,
	"post_phase_wnd_min[4]":	402,
	"post_phase_wnd_min[5]":	403,
	"post_phase_wnd_min[6]":	404,
	"post_phase_wnd_min[7]":	405,
	"post_phase_wnd_max[0]":	406,
	"post_phase_wnd_max[1]":	407,
	"post_phase_wnd_max[2]":	408,
	"post_phase_wnd_max[3]":	409,
	"post_phase_wnd_max[4]":	410,
	"post_phase_wnd_max[5]":	411,
	"post_phase_wnd_max[6]":	412,
	"post_phase_wnd_max[7]":	413,
	"":	       414   // NULL Parameter - table terminator
};
	
	var paramAccessEnum = {
		all:0,
		ram:0x80,
		rom:0x40,
		auth:0xC0,
	};
	
	var isAuthorized = false;
	
	var paramTypeEnum = {
		unknown:0,
		int32:1,
		uint32:2,
		float32:3,
		bool:4,
	};
	
	var getType = function(id){
		return accessAndTypeInfo[id] & 0x7;
	};
	
	var checkAccess = function(id, access){
		return (accessAndTypeInfo[id] & access) > 0;
	};
	
	var getConverter = function(t, isToBytes){
		if (t == paramTypeEnum.int32)
			return (isToBytes)? i2ab : ab2i;
		if (t == paramTypeEnum.uint32)
			return (isToBytes)? ui2ab : ab2ui;
		if (t == paramTypeEnum.float32)
			return (isToBytes)? f2ab : ab2f;
		if (t == paramTypeEnum.bool)
			return function(v,of){
				if (isToBytes)
					return ui2ab(+v,of);
				else
					return ab2ui(v,of)>0;
			}
	}
	
	desc.Initialize = function(){
/* 		    uint32_t nHeaderSize;
			uint32_t pParamTableAddr;
			uint32_t nParamCount;
			uint32_t nTypeDescAddr;
			uint32_t nFirmwareVersion;
			uint32_t pDefParamTableAddr; */
	
	
		var data = new Uint8Array(requestsController.Dev_ReadData(desc.pointer, 4));
		var headerSize = ab2i(data);
		if (headerSize == 6*4){ // from Device.h
			data = requestsController.Dev_ReadData(desc.pointer+4, 5*4);
			var idata = ab2ai(data);
			
			desc.header = {
				paramTableAddr: idata[0],
				paramCount: idata[1],
				typeDescAddr: idata[2],
				firmwareVersion: idata[3],
				defParamTableAddr: idata[4],
			};
			
			accessAndTypeInfo = new Uint8Array(read(desc.header.typeDescAddr, desc.header.paramCount));
		}
	}
	
	desc.SetAuthState = function(isAuth) {
		isAuthorized = isAuth;
	};
	
	desc.Param = function(name){
		var id = paramNameDictionary[name];
		var param = {
			id: id,
			valtype: getType(id),
			value: 0
		};
		
		var getArray = function(pointer, size){
			var u8data = new Uint8Array(read(pointer, size*4));
			
			var arr = [];
			
			for(var i = 0;i<size; i++){
				arr.push(getConverter(getType(id+i))(u8data.buffer, i*4));
			}
			
			param.value = arr.concat([]);
			
			return arr;
		};
		
		var setArray = function(pointer, array){
			for(var i = 0;i<array.length; i++){
				if (param.value[i] != array[i]){
					param.value[i] = array[i];
					
					var dataid = param.id + i;
					var datapointer = pointer + i*4;
					var data = getConverter(getType(dataid),true)(array[i]);
					
					write(datapointer, data);
				}
			}
		};
		
		param.GetArray = function(size){
			var pointer = desc.header.paramTableAddr + id*4;
			return getArray(pointer, size);
		};
		
		param.SetArray = function(array){
			var pointer = desc.header.paramTableAddr + id*4;
			setArray(pointer, array);
		};
		
		param.GetDefArray = function(size){
			var pointer = desc.header.defParamTableAddr + id*4;
			return getArray(pointer, size);
		};
		
		param.SetDefArray = function(array){
			var pointer = desc.header.defParamTableAddr + id*4;
			setArray(pointer, array);
		};
		
		return param;
	}
	
	return desc;
}

