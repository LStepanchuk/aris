function AnalyzerController() {
    var self = this;

    var i2ab = dataController.int2ab;
    var ui2ab = dataController.uint2ab;
    var f2ab = dataController.float2ab;

    var ab2i = dataController.ab2int;
    var ab2ui = dataController.ab2uint;
    var ab2f = dataController.ab2float;

    var ab2ai = dataController.ab2aint;
    var ab2af = dataController.ab2afloat;

    var read = requestsController.Dev_ReadData;
    var write = requestsController.Dev_WriteData;

	var analyzerRunChannel = null;
	var analyzerRecCount = null;
	var analyzerRecNo = null;
	var analyzerDataPart = null;
	
    var callbackPoints = null;
    var intervalHandle = null;
	
    var receivedPoints1 = [];
    var receivedPoints2 = [];
    var previosReceivedPoints1 = [];
    var previosReceivedPoints2 = [];
	
	var runChannel = -1;
	var lastReadPoint = 0;

    self.debug_maxTime = 0;
	
    var debug_time;
    var debug_timer_start = function() {
        debug_time = new Date();
    };

    var debug_timer_max = function() {
        self.debug_maxTime = Math.max((new Date() - debug_time) / 1000, self.debug_maxTime);
    }

	var getRunChannel = function(force){
		if (runChannel == -1 || force){
			runChannel = analyzerRunChannel.GetArray(1)[0];
		}
		return runChannel;
	};
	
    var reset = function() {
		receivedPoints1 = [];
		receivedPoints2 = [];
		previosReceivedPoints1 = [];
		previosReceivedPoints2 = [];
        lastReadPoint = 0;
    };

	var getDataPart = function(count){
		var values = analyzerDataPart.GetArray(16*3);
		var points = [];
		for(var i=0;i<count;i++){
			points.push({
				f: values[i],
				a: values[i+16],
				of: values[i+32],
			});
		}
		return points;
	};

    var getLastData = function() {
        var originalLastPoint = analyzerRecCount.GetArray(1)[0];
		var count = originalLastPoint - lastReadPoint;
		
		analyzerRecNo.SetArray([lastReadPoint]);
		
		var points = [];
		
		var offset = 0;
		do{
			var currentCount = Math.min(count-offset,16);
			var readPoints = getDataPart(currentCount);
			points = points.concat(readPoints);
			offset += currentCount;
			if (offset<count){
				analyzerRecNo.SetArray([lastReadPoint+offset]);
			}
		}while(offset<count);
		
		lastReadPoint += count;
		
		return points;
    };
	
	var capturePoints = function(){
		var points = getLastData();
		
		if (points.length>0){
			for(var i=0; i < points.length; i++){
				receivedPoints1.push({
					f: points[i].f,
					v: points[i].a,
				});
				receivedPoints2.push({
					f: points[i].f,
					v: points[i].of,
				});
			}
			
			if (callbackPoints){
				callbackPoints(receivedPoints1, receivedPoints2, previosReceivedPoints1, previosReceivedPoints2);
			}
		}
	}

    var routine = function() {
		if (getRunChannel() == 0)
			return;
			
		if (getRunChannel(true) == 0)
			return;
			
		capturePoints();
    };

    self.Initialize = function() {
		analyzerRunChannel = paramController.Param("analyzer_run_ch");
		analyzerRecCount = paramController.Param("analyzer_rec_count");
		analyzerRecNo = paramController.Param("analyzer_rec_no");
		analyzerDataPart = paramController.Param("analyzer_rec_freq[0]");
    };

    self.Start = function(callback) {
        callbackPoints = callback;
        reset();
        clearInterval(intervalHandle);
        intervalHandle = setInterval(routine, 500);
		capturePoints();
    };

    self.Stop = function() {
        clearInterval(intervalHandle);
    };
	
	//===Analyzer===
	
	self.GetAnalyzerChannel = function() {
		var runCh = getRunChannel();
		if (runCh > 8){
			return runCh - 9;
		}
		else {
			return runCh - 1;
		}
    };
	
	self.IsAnalyzerServoIn = function() {
		var runCh = getRunChannel();
		return runCh > 8;
    };
	
	self.IsAnalyzerStarted = function() {
		var runCh = getRunChannel();
		return runCh > 0;
    };
	
	self.AnalyzerStart = function(channel, servoIn) {
		analyzerRunChannel.SetArray([+channel+servoIn*8+1]);
		runChannel = +channel+servoIn*8+1;
		
		if (getRunChannel(true) == 0)
			return;
		
		previosReceivedPoints1 = receivedPoints1;
		previosReceivedPoints2 = receivedPoints2;
		receivedPoints1 = [];
		receivedPoints2 = [];
		lastReadPoint = 0;
    };
	
	self.AnalyzerStop = function() {
		analyzerRunChannel.SetArray([0]);
		runChannel = 0;
    };
	
	//======
	
    return self;
}