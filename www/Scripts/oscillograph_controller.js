function OscillographController(desc) {
    var self = this;

    if (desc.name != "@Oscilloscope")
        return null;

    var i2ab = dataController.int2ab;
    var ui2ab = dataController.uint2ab;
    var f2ab = dataController.float2ab;

    var ab2i = dataController.ab2int;
    var ab2ui = dataController.ab2uint;
    var ab2f = dataController.ab2float;

    var ab2ai = dataController.ab2aint;
    var ab2af = dataController.ab2afloat;

    var read = requestsController.Dev_ReadData;
    var write = requestsController.Dev_WriteData;

    var UI_POINTS_MAX = 400;
    var DATA_POINTS_PER_SEC = 10000;

    var callbackPoints = null;
	
    var osciloscopeTimeStamp = null;
    var osciloscopeChannel = null;
	
	
    var lastOsciloscopeTimeStamp = -1;
    var intervalHandle = null;
    var requestedInterval = 1;
    var currentInterval = 1;
    var lastPointTimeStamp = 0;
    var receivedPoints = [];

    desc.debug_maxTime = 0;
    //desc.debug_maxTime = Math.max((new Date() - time)/1000, desc.debug_maxTime);

    var debug_time;
    var debug_timer_start = function() {
        debug_time = new Date();
    };

    var debug_timer_max = function() {
        desc.debug_maxTime = Math.max((new Date() - debug_time) / 1000, desc.debug_maxTime);
    }

    var reset = function() {
        receivedPoints = [];
        currentInterval = requestedInterval;
        lastPointTimeStamp = 0;
    };

    var receiver = function(points) {
        receivedPoints = receivedPoints.concat(points);

        if (lastPointTimeStamp >= currentInterval) {
            var period = Math.floor(receivedPoints.length / UI_POINTS_MAX);
            var responce = [];

            for (var i = 0; i < receivedPoints.length && responce.length < UI_POINTS_MAX && receivedPoints[i].t < currentInterval; i += period) {
                responce.push(receivedPoints[i]);
            }

            reset();
            callbackPoints(responce);
        }
    };


    var isBufferUpdated = function() {
        var ts = osciloscopeTimeStamp.GetArray(1)[0];
        if (lastOsciloscopeTimeStamp != ts) {
            lastOsciloscopeTimeStamp = ts;
            return true;
        }
        return false;
    };

    var routine = function() {
        if (!isBufferUpdated()) {
            return;
        }

        var data = requestsController.Dev_ReadLongData(desc.pointer, desc.size);

        var values = ab2af(data);
        var points = [];
        for (var i = 0; i < values.length; i++) {
            points[i] = {
                t: lastPointTimeStamp,
                v: values[i]*3.3/65536,
            };
            lastPointTimeStamp += 1 / DATA_POINTS_PER_SEC;
        }
        receiver(points);
    };

    desc.Initialize = function() {
        osciloscopeTimeStamp = paramController.Param("oscil_ts");
		osciloscopeChannel = paramController.Param("oscil_ch");
    };

    desc.Start = function(callback) {
        callbackPoints = callback;
        reset();
        clearInterval(intervalHandle);
        intervalHandle = setInterval(routine, 10);
    };

    desc.Stop = function() {
        clearInterval(intervalHandle);
    };

    desc.SetInterval = function(i) {
        requestedInterval = i;
    };
	
	desc.GetInterval = function(i) {
        return requestedInterval;
    };
	
	desc.SetChannel = function(ch, isIn) {
        osciloscopeChannel.SetArray([+ch+isIn*8]);
    };
	
	desc.GetChannel = function() {
        var data = osciloscopeChannel.GetArray(1);
		return {
			ch: data%8,
			isIn: data>=8
		};
    };

    return desc;
}
