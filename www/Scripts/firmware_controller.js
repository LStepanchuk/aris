function FirmwareController(desc) {
    var self = this;

    if (desc.name != "@FW Storage")
        return null;

    var i2ab = dataController.int2ab;
    var ui2ab = dataController.uint2ab;
    var f2ab = dataController.float2ab;

    var ab2i = dataController.ab2int;
    var ab2ui = dataController.ab2uint;
    var ab2f = dataController.ab2float;

    var ab2ai = dataController.ab2aint;
    var ab2af = dataController.ab2afloat;

    var read = requestsController.Dev_ReadData;
    var write = requestsController.Dev_WriteData;
	
	var filedata;
	
	var load = function(useFactory){
		if (filedata){
			var configData = new Uint32Array(filedata, 0x00004000, 7);
			if (configData[0] == 0x494D4454){
				configData[5] = filedata.byteLength;
				configData[6] = +useFactory;
				
				var md5 = MD5(filedata);
				md5 = new Uint32Array(md5);
				
				configData[1] = md5[0];
				configData[2] = md5[1];
				configData[3] = md5[2];
				configData[4] = md5[3];
				
				return true;
			}
		}
		
		return false;
	};

    desc.Initialize = function() {
    };
	
	desc.ReadFileCallback = function(e){
		var file = e.target.files[0];
		if (!file) {
			return;
		}

		var reader = new FileReader();
		reader.onload = function(e) {
			filedata = e.target.result;
		};
		
		reader.readAsArrayBuffer(file);
	};
	

	
	desc.Upload = function(useFactory, callback){
		if (filedata && load(useFactory)){
			//Warning! Pasha permission required
			requestsController.Dev_WriteLongDataAsync(desc.pointer, filedata, callback);
			console.log("Firmware sent: " + filedata.byteLength);
		}
	}
	
    return desc;
}