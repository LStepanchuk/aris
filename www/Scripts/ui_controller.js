function UIController() {
    var self = this;
    var memoryType = 0; //0-ram, 1 = eeprom
    var timeInterval = 0.6;
    var levelInterval = 0.4;
    var startAnalyzerGraphics = false;
	var isUserMode = false;

    var plotOscilloscopeOptions = {
        series: {
            lines: {
                show: true,
                lineWidth: 1,
                lineColor: "green"
            }
        },
        xaxis: {
            font: {
                size: 11,
                style: "italic",
                weight: "bold",
                family: "sans-serif",
                variant: "small-caps",
                color: "#00FF00",
            }
        },
        yaxis: {

            //min: -4,
            //max: 4,

            font: {
                size: 11,
                style: "italic",
                weight: "bold",
                family: "sans-serif",
                variant: "small-caps",
                color: "#00FF00",

            }

        },
        legend: {
            show: false,
        },
        grid: {
            color: "#00FF00",
            textColor: "#00FF00",
            hoverable: true,
            clickable: true
        }

    };


    var plotOptions = {
        series: {
            lines: {
                show: true,
                lineWidth: 1,
                //lineColor: "green"
            }
        },
        xaxis: {
            font: {
                size: 11,
                style: "italic",
                weight: "bold",
                family: "sans-serif",
                variant: "small-caps",
                color: "black",
            },
            ticks: [0.001, 0.01, 0.1, 1, 10, 100],
            transform: function(v) {
                return Math.log(v + 0.0001); /*move away from zero*/
            },
            tickDecimals: 3
        },
        yaxis: {

            //min: -4,
            //max: 4,

            font: {
                size: 11,
                style: "italic",
                weight: "bold",
                family: "sans-serif",
                variant: "small-caps",
                color: "black"
            },
            legend: {
                show: false,
            },
            grid: {
                color: "black",
                textColor: "black",
                hoverable: true,
                clickable: true
            }
        }
    };


    function HideUpperWindows() {
        $(".js-hide").hide();
    }

    function UpdateSelectLists(data) {
        $(".js-select-list").empty();
        $.each(data, function(i, val) {
            AddSelectListOption(val, i); // append item to list
        });
        $(".js-select-list option:first").attr('selected', 'selected');
    }

    function AddSelectListOption(val, i) {
        $(".js-select-list").append($('<option>', {
            value: i,
            text: val.name
        }));
    }



    function UpdateUpperWindow(data) {
        $(".js-half-part-item").remove();
        var parrent;
        $.each(data, function(i, val) {
            if (i < 4) {
                parrent = $(".js-half-part-left");
            } else {
                parrent = $(".js-half-part-right");
            }
            var proto = $('.js-half-part-item-proto');
            var clone = proto.clone();
            clone.addClass('js-half-part-item').removeClass('js-half-part-item-proto');
            clone.attr("id", val.name);
            clone.find('.js-change-chanel-names').attr("value", val.name); //
            clone.find('.js-label').text("Chanel " + (i + 1) + ":"); //
            clone.show();
            clone.appendTo(parrent);
        });
    }

    function UpdateParemetersTab(data) {
        $(".js-params-panel-item").remove();

        $.each(data, function(i, val) {
            var proto = $('.js-params-panel-item-proto');
            var clone = proto.clone();
            clone.addClass('js-params-panel-item').removeClass('js-params-panel-item-proto');
            clone.attr("id", val.name);
            clone.find('.js-gain-input').attr("value", val.gain);
            clone.find('.js-gain-input').attr("id", val.gain);
            clone.find('.js-label').text(val.name);
            clone.find('.js-check').prop('checked', val.switch);
            clone.find('.js-check').attr("id", val.switch);
            clone.show();
            clone.appendTo('.js-params-panel');
        });
    }


    function stringToBoolean(string) {
        switch (string.toLowerCase().trim()) {
            case "true":
            case "yes":
            case "1":
                return true;
            case "false":
            case "no":
            case "0":
            case null:
                return false;
            default:
                return Boolean(string);
        }
    }
	
    function UpdateChanelsUI() {
        var set = dataController.GetChanelsSet();
        UpdateSelectLists(set);
        UpdateUpperWindow(set);
        UpdateParemetersTab(set);
    }

    function GetFiltersDataFromUI() {
        var filters = [];
        $('.js-filter-tab-item').each(function() {
            filters.push($(this).val());
        });
        return filters;
    }
	
	function ConvertUintToNames(arr){
		var res = [];
		
		for(var i in arr){
			var data = dataController.uint2ab(arr[i]);
			var chars = new Uint8Array(data).filter(function(w){ return w>0;});
			var str = dataController.ab2str(new Uint8Array(chars).buffer);
			res.push(str);
		}
		
		return res;
	}
	
	function UpdateChannelNames(){
		var channelNamesPar = paramController.Param("ch0_name");
		var names = ConvertUintToNames(channelNamesPar.GetArray(8));
		$(".js-filters-select").each(function(i){
			$("option",$(this)).each(function(j){
				$(this).text(names[j]);
			});
		});
	}
	
	function rd(x){
		return Math.round(x*10000)/10000;
	}
	
	
	
	
	
	//==============================================================================================
    function InitCommonElements() {
	
        $('.js-select-list').on('change', function() {
            var val = $(this).val();
        });
		
        $(".js-cancel-btn").click(function() {
            HideUpperWindows();
        });
		
		UpdateChannelNames();
    }
	
	
	
	
	
	
	
	//==============================================================================================
    function InitLoginPage() {
		
		function setDefaultLoginData() {
			var user = dataController.GetUserData();
			$(".js-login-input").val(user.name);
			$(".js-password-input").val(user.password);
		}
		
		function setUserMode(set){
			isUserMode = set;
			$(".js-login-input").prop("disabled", isUserMode);
			$(".js-password-input").prop("disabled", isUserMode);
		}
	
        $(".js-connect").click(function() {
            //Check data
            //connect
            //set usertype marker
            //dataController.StartConnection();

            var login = $(".js-login-input").val();
            var password = $(".js-password-input").val();

            if (isUserMode || authorizationController.Authorize(login, password)) {
                $(".js-login").hide();
                $(".js-developer").show();
                Connected();
            }else{
				self.Alert("Invalid login or password");
			}
            //var res = GetFiltersDataFromUI();
        });

        $(".js-change").click(function() {
            $(".js-dark-screen").show();
            $(".js-pass-change").show();
            var user = dataController.GetUserData();
            $(".js-login-input-old").val(user.name);
            $(".js-login-password-old").val(user.password);

            $(".js-login-input-new").val("");
            $(".js-login-password-new").val("");
        });
		
		$(".js-login-radio-user").change(function(){
			setUserMode(true);
		});
		$(".js-login-radio-dev").change(function(){
			setUserMode(false);
		});
		
		$(".js-ok-btn",".js-pass-change").click(function(){
			var oldlogin = $(".js-login-input-old").val();
            var oldpassword = $(".js-login-password-old").val();
			var newlogin = $(".js-login-input-new").val();
            var newpassword = $(".js-login-password-new").val();
			
			if (authorizationController.ChangeCredentials(oldlogin,oldpassword,newlogin,newpassword)){
				self.Alert("Login and password has been changed");
			} else {
				self.Alert("Invalid old login or password");
			}
			
			HideUpperWindows();
		});
		
		$(".js-button-about").prop("disabled",true);
		//$(".js-change").prop("disabled",true);
		$(".js-login-radio-dev").prop("checked", true);
		
		setDefaultLoginData();
		setUserMode(false);
		
		authorizationController.RegisterDisconnectCallback(function(){
			self.Fatal("Disconnect");
		});
    }
	
	
	
	
	
	
	
	//==============================================================================================
    function InitFirmwarePage() {
        $("input[type=file]").change(firmwareController.ReadFileCallback);

        $(".js-up-firmware-btn").click(function() {
            firmwareController.Upload($(".js-check-def-factory").prop("checked"), function(e) {
                $(".js-file-progressbar").val(e.total * 100 / e.size);

                if (e.total == e.size) {
                    if (confirm("Now the device will be asked to reboot.\nIf it does not - the firmware was not updated successfuly!")) {
                        requestsController.Dev_Leave();
						self.ReloadUI();
                    }
                }
            });
        });
		
		$(".js-file-progressbar").val(0);
    }
	
	
	
	
	
	
	
	//==============================================================================================
    function InitFilterSettingsPage() {
				
		var filterChannelPar = [];
		var currentChannel = 0;
		var isRam = true;
	
		function createFiltersPar(){
			for(var i=0; i<8; i++){
			   filterChannelPar[i] = paramController.Param("ch" + i + "_cf_N[0]");
			}
		}
		
		function createFilters() {
			var rowProto = $('.js-tab-row-proto');
			var proto = $('.js-tab-cell-proto');
			var columnOrder = [0,2,1,3];
			for (var i = 0; i < 8; i++) {
				var rowClone = rowProto.clone();
				rowClone.addClass('js-tab-row-item').removeClass('js-tab-row-proto');
				rowClone.find(".js-filter-header").html("Filter" + (i + 1));
				rowClone.show();
				rowClone.appendTo('.js-settings-table');
				
				for (var c = 0; c < 4; c ++) {
					var clone = proto.clone();
					clone.attr("index",columnOrder[c]*8+i);
					clone.addClass('js-tab-cell-item').removeClass('js-tab-cell-proto');
					clone.show();
					clone.appendTo(rowClone);
				}
			}
		}
		
		function updateFilters(data){
			$(".js-tab-cell-item",".js-filter-settings-div").each(function(){
				var id = +$(this).attr("index");
				$("input",$(this)).val(rd(data[id]));
			});
		}
		
		function gatherFilters(){
			var res = [];
			$(".js-tab-cell-item",".js-filter-settings-div").each(function(){
				var id = +$(this).attr("index");
				res[id] = +$("input",$(this)).val();
			});
			return res;
		}
		
		
		if (isUserMode){
			$("#notebook1_4").remove();
			$("label[for=notebook1_4]").remove();
			return;
		}
	
        $('.js-filters-select').on('change', function() {
			currentChannel = +$(this).val();
        });

        $(".js-set-filters-data-btn").click(function() {
            if (isRam){
				filterChannelPar[currentChannel].GetArray(32);
				filterChannelPar[currentChannel].SetArray(gatherFilters());
			}else{
				filterChannelPar[currentChannel].GetDefArray(32);
				filterChannelPar[currentChannel].SetDefArray(gatherFilters());
			}
        });
		
		$(".js-show-filters-btn").click(function() {
			var data;
            if (isRam)
				data = filterChannelPar[currentChannel].GetArray(32);
			else
				data = filterChannelPar[currentChannel].GetDefArray(32);
			updateFilters(data);
        });

        $(".js-mem-type-btn").click(function() {
            $(".js-mem-type-btn").removeClass('ui-mem-type-btn-active');
			isRam = this.id == 0;
            $(this).addClass('ui-mem-type-btn-active');
        });
		
		createFiltersPar();
		createFilters();
		updateFilters(filterChannelPar[currentChannel].GetArray(32));
    }
	
	
	
	
	
	
	
	//==============================================================================================
    function InitAnalyzerPage() {
		var lastPoints1;
		var lastPreviosPoints1;
		var lastPoints2;
		var lastPreviosPoints2;
	
	
		function disableRuntimeUi(disable){
			$(".js-analyzer-selected-list",".ui-left-analyzer-div").prop("disabled",disable);
			$(".js-settings-radio",".ui-left-analyzer-div").prop("disabled",disable);
		}
		
		function updateAnalyzerStateStart(){
			var  seroIn = analyzerController.IsAnalyzerServoIn();
			$(".js-settings-radio",".ui-left-analyzer-div").each(function(i){
				if (+$(this).val() == +seroIn)
					$(this).prop("checked",true);
			});
			updateAnalyzerState();
		}
	
		function updateAnalyzerState(){
			var  channel = analyzerController.GetAnalyzerChannel();
			var channelName = "";
			
			$("option",".js-analyzer-selected-list").each(function(i){
				if ($(this).val() == channel){
					$(this).prop("selected",true);
					channelName = $(this).text();
				}
			});
			
			if (analyzerController.IsAnalyzerStarted()){
				$('.js-chanel-start-btn').val("Stop");
				$(".js-chanel-state").text("RUN ch:"+channelName);
				disableRuntimeUi(true);
			}else{
				$('.js-chanel-start-btn').val("Start");
				$(".js-chanel-state").text("Ready");
				disableRuntimeUi(false);
			}
		}
		
		function drawGraphics(points1, points2, parent) {
			//var data = dataController.GetOscilloscopeGraphicData();
			var array1 = [];
			var array2 = [];

			$.each(points1, function(i, point) {
				array1.push([point.f, point.v]);
			});

			if ($(".js-chanel-check").prop("checked")) {
				$.each(points2, function(i, point) {
					array2.push([point.f, point.v]);
				});
			}

			var graph1 = {
				data: array1,
				label: "",
				color: "blue"
			};
			var graph2 = {
				data: array2,
				label: "",
				color: "red"
			};; //sampleFunction( 0, 5, function(x){ return Math.exp(x)*Math.sin(x)*Math.sin(x) } ); ////

			$.plot(parent, [graph2, graph1, ], plotOptions);
		}
		
		if (isUserMode){
			$("#notebook1_5").remove();
			$("label[for=notebook1_5]").remove();
			return;
		}
	
        $(".notebook_radio").click(function() {
            if (this.id == 'notebook1_5') {
                analyzerController.Start(function(recPoints1, recPoints2, prevPoints1, prevPoints2) {
                    drawGraphics(recPoints1, prevPoints1, ".js-moved-graphic-gain");
                    drawGraphics(recPoints2, prevPoints2, ".js-moved-graphic-phase");
					
					lastPoints1 = recPoints1;
					lastPreviosPoints1 = prevPoints1;
					lastPoints2 = recPoints2;
					lastPreviosPoints2 = prevPoints2;
					
					$(".js-chanel-data").text(recPoints1.length+" point(s)");
                });
				
				updateAnalyzerStateStart();
            } else {
                analyzerController.Stop();
            }
        });

        $('.js-chanel-start-btn').click(function() {
            if (!analyzerController.IsAnalyzerStarted()) {
                analyzerController.AnalyzerStart($(".js-analyzer-selected-list").val(), $('input[name="chanelRadio"]:checked').val());
            } else {
                analyzerController.AnalyzerStop();
            }
			updateAnalyzerState();
        });
		
		$('.js-chanel-check').click(function() {
			if (lastPoints1){
				drawGraphics(lastPoints1, lastPreviosPoints1, ".js-moved-graphic-gain");
				drawGraphics(lastPoints2, lastPreviosPoints2, ".js-moved-graphic-phase");
			}
        });
		
		$(".js-chanel-save-btn").prop("disabled",true);
		$(".js-chanel-print-btn").prop("disabled",true);
    }
	
	
	
	
	
	
	
	//==============================================================================================
    function InitOscilloscopePage() {
	
		function drawOscilloscopeGraphic(points) {
			//var data = dataController.GetOscilloscopeGraphicData();
			var array = [];
			$.each(points, function(i, point) {
				array.push([point.t, point.v]);
			});

			var graph = {
				data: array,
				label: "",
				color: "#00FF00"
			}
			$.plot(".js-oscilloscope", [graph], plotOscilloscopeOptions); //d1, d2, d3  
		}
	
	    function startOsciloscope() {
			oscillographController.SetInterval(timeInterval);
			oscillographController.Start(function(points) {
				drawOscilloscopeGraphic(points);
			});
		}
	
		function updateOscilographState(){
			var cahnnelServ = oscillographController.GetChannel();
		
			$("option",".ui-right-oscilloscope .js-select-list").each(function(i){
				if ($(this).val() == cahnnelServ.ch){
					$(this).prop("selected",true);
				}
			});
			
			var inp = $("input[value=Input]",".ui-right-oscilloscope");
			var outp = $("input[value=Output]",".ui-right-oscilloscope");
			inp.removeClass("ui-mem-type-btn-active");
			outp.removeClass("ui-mem-type-btn-active");
			
			if (cahnnelServ.isIn)
				inp.addClass("ui-mem-type-btn-active");
			else
				outp.addClass("ui-mem-type-btn-active");
			
			val = 0.4;
			$(".js-level-scroll").val(Math.log(val/0.001889723)/0.46297774);
			plotOscilloscopeOptions.yaxis.max = val;
			plotOscilloscopeOptions.yaxis.min = -val;
			
			val = oscillographController.GetInterval();
			
			$(".js-time-scroll-label").html(val);
			$(".js-time-scroll").val(val);
		}
			
		function channelChanged(){
			var ch = +$(".js-select-list",".ui-right-oscilloscope").val();
			var isIn = $("input[value=Input]",".ui-right-oscilloscope").hasClass("ui-mem-type-btn-active");
			
			oscillographController.SetChannel(ch, isIn);
		};
		
		function buttonClicked(){
			var inp = $("input[value=Input]",".ui-right-oscilloscope");
			var outp = $("input[value=Output]",".ui-right-oscilloscope");
			inp.removeClass("ui-mem-type-btn-active");
			outp.removeClass("ui-mem-type-btn-active");
			
			$(this).addClass("ui-mem-type-btn-active");
			channelChanged();
		}
		
        $(".js-osc-btn").click(function() {
            $(".js-dark-screen").show();
            $(".js-graphics").show();
            startOsciloscope();

        });
		
        $(".js-close-micro").click(function() {
            oscillographController.Stop();
            HideUpperWindows();
        });

        $(".js-level-scroll").on('input',function(e) {
			var val = +$(this).val();
			val = 0.001889723*Math.exp(0.46297774*val); // todo: use constant gradation 
			//  Math.log(val/0.001889723)/0.46297774;
			val = Math.round(val*1000)/1000;
            $(".js-level-scroll-label").html(val);
			
			plotOscilloscopeOptions.yaxis.max = val;
			plotOscilloscopeOptions.yaxis.min = -val;
        });

        $(".js-time-scroll").on('input',function(e) {
			var val = +$(this).val();
            $(".js-time-scroll-label").html(val);
			
			oscillographController.SetInterval(val);
        });
		
		$(".js-select-list",".ui-right-oscilloscope").change(channelChanged);
		$("input[value=Input]",".ui-right-oscilloscope").click(buttonClicked);
		$("input[value=Output]",".ui-right-oscilloscope").click(buttonClicked);
		
		updateOscilographState();
    }
	
	
	
	
	
	
	
	//==============================================================================================
    function InitParametersPage() {

		//=====================================
		function initChannelNamesChange(){
			//"ch0_name"
			
			var channelNamesPar = paramController.Param("ch0_name");
			var channelNames;
			
			function convertToUint(arr){
				var res = [];
				
				for(var i in arr){
					var uint = 0;
					var data = new Uint8Array(dataController.str2ab(arr[i]));

					for (var j = 0; j < data.length; j++) {
						uint |= data[j] << (8 * j);
					}
					res.push(uint);
				}
				
				return res;
			}
			
			function updateChannelNames(){
				channelNames = ConvertUintToNames(channelNamesPar.GetDefArray(8));
				
				$(".js-label", ".js-params-panel").each(function(i){
					$(this).text(channelNames[i]);
				});
				
				$(".js-change-chanel-names", ".js-names-change").each(function(i){
					$(this).val(channelNames[i]);
				});
			}
			
			$(".js-label", ".js-params-panel").first().remove();
			$(".js-change-chanel-names", ".js-names-change").first().remove();
			
			updateChannelNames();
			
			$(".js-config-chanel-btn").click(function() {
				$(".js-dark-screen").show();
				$(".js-names-change").show();
				updateChannelNames();
			});
			
			$(".js-ok-btn", ".js-names-change").click(function() {
				$(".js-change-chanel-names", ".js-names-change").each(function(i){
					channelNames[i] = $(this).val();
				});
				var values = convertToUint(channelNames);
				channelNamesPar.SetDefArray(values);
				updateChannelNames();
				
				HideUpperWindows();
				UpdateChannelNames();
			});
		}
		
		
		//=====================================
		function initNetworkSettingsChange(){
/* 			"net_ip":      338,
			"net_mask":    339,
			"net_gateway": 340,
			"net_port":    341,
			"net_mac_0":   342,
			"net_mac_1":   343, */
		
			var networkPar = paramController.Param("net_ip");
			
			function parseIp(ip){
				var ipRegexp = /[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}/;
				var res = ipRegexp.exec(ip);
				if (res)
					return res[0];
				return null;
			}
			
			function ipToUint(ip){
				var ip = parseIp(ip);
				if (ip == null)
					return -1;
			
				var splited = ip.split(".");
				var val = [+splited[0],+splited[1],+splited[2],+splited[3]];
				
				if (val.find(function(v){ return v<0 || v>255;}))
					return -1;
					
				return (val[0]<<24) | (val[1]<<16) | (val[2]<<8) | val[3];
			}
			
			function uintToIp(uint){
				var bytes = new Uint8Array(dataController.uint2ab(uint));
				return bytes.reverse().join(".");
			}
			
			function macToU2uint(macArr){
				var val = [];
				for(var i in macArr){
					val[i] = parseInt(macArr[i],16);
					if (isNaN(val[i]))
						return null;
				}
			
				var uint1 = (val[3]<<24) | (val[2]<<16) | (val[1]<<8) | val[0];
				var uint2 = (val[5]<<8) | val[4];
				return [uint1, uint2];
			}
			
			function uint2ToMac(uint1, uint2){
				var bytes1 = new Uint8Array(dataController.uint2ab(uint1));
				var bytes2 = new Uint8Array(dataController.uint2ab(uint2));
				return new Uint8Array(dataController.appendab(bytes1.buffer, bytes2.buffer));
			}
			
			function updateNetworkSettings(){
				//js-change-settings-ip
				//js-change-settings-mask
				//js-change-settings-gateway
				//js-change-settings-tcp
				//js-mac-address-item
				var networkValues = networkPar.GetDefArray(6);
				$(".js-change-settings-ip").val(uintToIp(networkValues[0]));
				$(".js-change-settings-mask").val(uintToIp(networkValues[1]));
				$(".js-change-settings-gateway").val(uintToIp(networkValues[2]));
				$(".js-change-settings-tcp").val(networkValues[3]);
				
				var macArr = uint2ToMac(networkValues[4], networkValues[5]);
				$(".js-mac-address-item").each(function(i){
					var val = macArr[i].toString(16);
					$(this).val(((val.length==1)?"0":"")+val);
				});
			}
			
			function createMacInput(){
				var proto = $('.js-mac-address-proto');
				for (var i = 0; i < 6; i++) {
					var clone = proto.clone();
					clone.addClass('js-mac-address-item').removeClass('js-mac-address-proto');
					clone.show();
					clone.change(function(){
						if ($(this).val().length == 0) $(this).val("00");
						if ($(this).val().length == 1) $(this).val("0" + $(this).val());
					});
					clone.appendTo('.js-mac-address-container');
				}
			}
			
		
			$(".js-net-setting-btn").click(function() {
				$(".js-dark-screen").show();
				$(".js-device-network-setting").show();
				updateNetworkSettings();
			});
			
			$(".js-ok-btn", ".js-device-network-setting").click(function() {
				var uintIp = ipToUint($(".js-change-settings-ip").val());
				if (uintIp == -1){
					self.Alert("Error in IP");
					return;
				}
				
				var uintMask = ipToUint($(".js-change-settings-mask").val());
				if (uintMask == -1){
					self.Alert("Error in Mask");
					return;
				}
				
				var uintGateway = ipToUint($(".js-change-settings-gateway").val());
				if (uintGateway == -1){
					self.Alert("Error in Gateway");
					return;
				}
				
				var port = +$(".js-change-settings-tcp").val();
				if (!port || port<0 || port>65535){
					self.Alert("Error in Port");
					return;
				}
				
				var macArr = [];
				
				$(".js-mac-address-item").each(function(i){
					macArr.push($(this).val());
				});
				
				var uintsMac = macToU2uint(macArr);
				if (!uintsMac){
					self.Alert("Error in Mac");
					return;
				}
				
				networkPar.SetDefArray([uintIp, uintMask, uintGateway, port, uintsMac[0], uintsMac[1]]);				
				HideUpperWindows();
			});
			
			createMacInput();
		}
		
		
		//=====================================
        function initGainSettings(){
/* 			"gain[0]":     256,
			"gain[1]":     257,
			"gain[2]":     258,
			"gain[3]":     259,
			"gain[4]":     260,
			"gain[5]":     261,
			"gain[6]":     262,
			"gain[7]":     263,
			"switch[0]":   264,
			"switch[1]":   265,
			"switch[2]":   266,
			"switch[3]":   267,
			"switch[4]":   268,
			"switch[5]":   269,
			"switch[6]":   270,
			"switch[7]":   271, */
			
			var gainPar = paramController.Param("gain[0]");
			var switchPar = paramController.Param("switch[0]");
			
			var gains;
			var switches;
			
			function updateGainSwitch(){
				gains = gainPar.GetArray(8);
				switches = switchPar.GetArray(8);
				
				$(".js-check", ".js-params-panel-item").each(function(i){
					$(this).prop("checked",switches[i]);
				});
				
				$(".js-gain-input", ".js-params-panel-item").each(function(i){
					$(this).val(rd(gains[i]));
				});
			}
		
			function gatherGainSwitch(){
				$(".js-check", ".js-params-panel-item").each(function(i){
					switches[i] = $(this).prop("checked");
				});
				
				$(".js-gain-input", ".js-params-panel-item").each(function(i){
					gains[i] = +$(this).val();
				});
			}
		
			$(".js-store-btn").click(function() {
				gatherGainSwitch();
				gainPar.SetDefArray(gains);
				switchPar.SetDefArray(switches);
			});

			$(".js-set-btn").click(function() {
				gatherGainSwitch();
				gainPar.SetArray(gains);
				switchPar.SetArray(switches);
			});
			
			updateGainSwitch();
		}

		
		
        
		$(".js-factory-btn").prop("disabled",true);
        $(".js-factory-btn").click(function() {
        });
		
		initChannelNamesChange();
		initNetworkSettingsChange();
		initGainSettings();
    }
	
	
	
	
	
	
	
    function Connected() {
        InitFirmwarePage();
        InitFilterSettingsPage();
        InitAnalyzerPage();
        InitOscilloscopePage();
        InitParametersPage();
    }


    self.HtmlInitElements = function() {		
        UpdateChanelsUI();
		
        InitCommonElements();
        InitLoginPage();
    };
	
	self.ReloadUI = function(){
		location.reload();
	}
	
	self.Alert = function(mess){
		alert(mess);
	}
	
	self.Fatal = function(mess){
		alert(mess);
		location.reload();
	}
}