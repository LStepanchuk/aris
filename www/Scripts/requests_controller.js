function RequestsController() {
    var self = this;
    var resource = "";

    function GetRequest(request, data) {
        console.log("GET request '" + request+data);
        var xhr = new XMLHttpRequest();
        xhr.open("GET", request + data, false);
        xhr.overrideMimeType("text\/plain; charset=x-user-defined");
        xhr.send();

        if (xhr.status != 200) {
            console.log("GET request '" + request + "' failed");
            return dataController.str2ab("");
        }
        return dataController.str2ab(xhr.responseText);
    };

    function PostRequest(request, data) {
        console.log("POST request '" + request);
        var xhr = new XMLHttpRequest();
        xhr.open("POST", request, false);
        xhr.send(data);

        if (xhr.status != 200) {
            console.log("POST request '" + request + "' failed");
			
			if (xhr.status == 404){
				uiController.Fatal("Internal device error");
			}
				
            return false;
        }
        return true;
    };

    self.Dev_Leave = function() {
        var promise = PostRequest("dev_leave", "0");
        return promise;
    };

    self.Dev_WriteData = function(nAddress, arrData) {
        if (dataController.CheckData(nAddress, arrData)) //replace to data controller later
            return false;
        var promise = PostRequest("write_data_" + nAddress.toString(16), arrData);
        return promise;
    };
	
	self.Dev_WriteLongDataAsync = function(nAddress, arrData, callback) {
        var offset = 0;
		
		// TODO: For debug. Use while(){...} with self.Dev_WriteData;
		function recursiveWhile(){
			var size = Math.min(arrData.byteLength - offset,1024);
			
			var data = new Uint8Array(arrData, offset, size);			
			self.Dev_WriteData(nAddress + offset, data);
			//console.log("Dev_WriteData", nAddress + offset, data.byteLength);
						
			offset += size;			
			
			if (callback)
				callback({ sent: size, total: offset, size: arrData.byteLength});
				
			if (offset < arrData.byteLength)
				setTimeout(recursiveWhile,1);
		}
		
		setTimeout(recursiveWhile,1);
		
		// do{
			// var size = Math.min(arrData.byteLength - offset,1024);
			
			// var data = new Uint8Array(arrData, offset, size);			
			// self.Dev_WriteData(nAddress + offset, data);
			////console.log("Dev_WriteData", nAddress + offset, data.byteLength);
						
			// offset += size;			
			
			// if (callback)
				// callback({ sent: size, total: offset, size: arrData.byteLength});
		// }while(offset<arrData.byteLength);
    };


    self.Dev_ReadDescriptorString = function(nStr) {
        if (typeof nStr != "number")
            return "";
        var promise = GetRequest("dev_desc_str_", nStr.toString(16));
        return promise;
    }

    self.Dev_ReadData = function(nAddress, nSize) {

        if (dataController.CheckFullData(nAddress, nSize))
            return dataController.str2ab("");

        var data = GetRequest("read_data_", nAddress.toString(16) + "_" + nSize.toString(16));
        return data;
    }
	
	self.Dev_ReadLongData = function(nAddress, nSize) {
		var data = (new Uint8Array([])).buffer;
		var offset = 0;
		
		do{
			var size = Math.min(nSize - offset,1024);
			var controlSize = data.byteLength;
			
			data = dataController.appendab(data, self.Dev_ReadData(nAddress + offset, size));
			
			if (controlSize == data.byteLength){
				console.log("Dev_ReadData return empty array");
				return data;
			}
			
			offset += size;
		}while(offset < nSize);
		
        return data;
    }

    /*this.Dev_ = function(){
				  var promise = ;
          promise.then(function (response) {
            return response;
          });
          return promise;
	}*/




}