//////////////////////////////////////////////////////////////////////////////////////////

/* const */ var ARISCP_MAX_DATALEN = 1024;

function str2ab(str)
{
	var buf = new ArrayBuffer(str.length);
	var bufView = new Uint8Array(buf);

	for(var i = 0, strLen = str.length; i < strLen; i++)
	{
		bufView[i] = str.charCodeAt(i) & 0xff;
	}

	return buf;
}

function appendab(buffer1, buffer2)
{
	var tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
	tmp.set(new Uint8Array(buffer1), 0);
	tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
	return tmp.buffer;
}

//////////////////////////////////////////////////////////////////////////////////////////
// Read device descriptor string
//////////////////////////////////////////////////////////////////////////////////////////

function Dev_ReadDescriptorString(nStr)
{
	if(typeof nStr != "number")
		return "";

	var xhr = new XMLHttpRequest();

	xhr.open("GET", "dev_desc_str_" + nStr.toString(16), false);
	xhr.overrideMimeType("text\/plain; charset=x-user-defined");
	xhr.send();
	
	if(xhr.status != 200)
		return "";
	
	return xhr.responseText;
}

for(var i = 0;; ++i)
{
	var str = Dev_ReadDescriptorString(i);
	
	if(str == "")
		break;

	document.write("Dev_ReadDescriptorString(" + i + ") : " + str + "<br>\n");
}

//////////////////////////////////////////////////////////////////////////////////////////
// Upload data
//////////////////////////////////////////////////////////////////////////////////////////

function Dev_ReadData(nAddress, nSize)
{
	if((typeof nAddress != "number") || (typeof nSize != "number") || nSize > ARISCP_MAX_DATALEN)
		return str2ab("");

	var xhr = new XMLHttpRequest();

	xhr.open("GET", "read_data_" + nAddress.toString(16) + "_" + nSize.toString(16), false);
	xhr.overrideMimeType("text\/plain; charset=x-user-defined");
	xhr.send();

	if(xhr.status != 200)
		return str2ab("");

	return str2ab(xhr.responseText);
}

var buf = Dev_ReadData(0x40000000, 1024);
buf = appendab(buf, Dev_ReadData(0x40000000 + 1024, 1024))
buf = appendab(buf, Dev_ReadData(0x40000000 + 1024 + 1024, 1024));
buf = appendab(buf, Dev_ReadData(0x40000000 + 1024 + 1024 + 1024, 1024));

var bufView = new Uint8Array(buf);

document.write("Dev_ReadData() : <br>\n");

for(var i = 0, len = bufView.length; i < len; i++)
	document.write(bufView[i].toString(16));
document.write("<br>\n");

//////////////////////////////////////////////////////////////////////////////////////////
// Download data
//////////////////////////////////////////////////////////////////////////////////////////

function Dev_WriteData(nAddress, arrData)
{
	// TODO: Validate type of arrData
	if((typeof nAddress != "number") || (arrData.length > ARISCP_MAX_DATALEN))
		return false;

	var xhr = new XMLHttpRequest();

	xhr.open("POST", "write_data_" + nAddress.toString(16), false);
	//xhr.overrideMimeType("text\/plain; charset=x-user-defined");
	xhr.send(arrData);

	if(xhr.status != 200)
		return false;

	return true;
}

var data = new Uint32Array(1);
data[0] = 0;

document.write("Dev_WriteData() : " + Dev_WriteData(0x200006B4, data) + "<br>\n");	// auth_cr

//////////////////////////////////////////////////////////////////////////////////////////
// Leave DFU Mode
//////////////////////////////////////////////////////////////////////////////////////////

function Dev_Leave()
{
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "dev_leave", false);
	//xhr.overrideMimeType("text\/plain; charset=x-user-defined");
	xhr.send(0);

	if(xhr.status != 200)
		return false;

	return true;
}

//document.write("Dev_Leave() : " + Dev_Leave() + "<br>\n");	// auth_cr
